import Form1 from "./components/Form1";
import Form2 from "./components/Form2";

const App = () => {
  return (
    <div className="container my-5 mx-auto">
      <h1 className="text-3xl font-bold underline mb-4 text-center">
        Agregar usuario
      </h1>

      {/* <Form1 /> */}
      <Form2 />
    </div>
  );
};

export default App;
