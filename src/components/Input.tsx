import React from "react";

interface props {
  type?: string;
  placeholder?: string;
  name?: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  value?: string;
  required?: boolean;
  label?: string;
  error?: string;
  id?: string;
}

const Input = ({
  type,
  placeholder,
  name,
  onChange,
  value,
  required,
  label,
  error,
  id,
}: props) => {
  return (
    <>
      <label htmlFor={id}>{label}</label>

      <input
        type={type}
        id={id}
        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
        placeholder={placeholder}
        name={name}
        onChange={onChange}
        // {...register(name, onChange)}
        value={value}
        required={required}
      />

      {error ? <p className="text-red-400">{error}</p> : null}
    </>
  );
};

export default Input;
