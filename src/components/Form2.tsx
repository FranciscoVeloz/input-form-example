import Input from "./Input";
import { ChangeEvent, FormEvent, useState } from "react";
import { IUser } from "../interfaces/IUser";
import { UserSchema } from "../schemas/user.schema";
import Button from "./Button";

//Estado inicial del form
const initialState: IUser = {
  fullname: "",
  email: "",
  password: "",
};

const Form2 = () => {
  //Use state para el form
  const [user, setUser] = useState<IUser>(initialState);

  //Manejamos el cambio de estado
  const handleChange = (e: ChangeEvent<HTMLInputElement>) =>
    setUser({ ...user, [e.target.name]: e.target.value });

  //Al dar click al boton enviamos los datos
  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const result = UserSchema.safeParse(user);
    if (!result.success) {
      result.error.errors.forEach((e) => alert(e.message));
      return;
    }

    console.log(user);
    setUser(initialState);
    alert("Usuario agregado con éxito");
  };

  const error = "Esto es un error";

  return (
    <form onSubmit={handleSubmit}>
      <div className="grid gap-6 mb-6">
        <Input
          label="Hola mundo"
          error={error ? error : ""}
          type="file"
          placeholder="Fullname"
          name="fullname"
          onChange={handleChange}
          value={user.fullname}
          id="input1"
          required
        />

        <Input
          label="Hola"
          type="text"
          placeholder="Email"
          name="email"
          onChange={handleChange}
          value={user.email}
          id="in"
          required
        />

        <Input
          type="password"
          placeholder="Password"
          name="password"
          onChange={handleChange}
          value={user.password}
          required
        />

        <Button type="submit">Submit</Button>
      </div>
    </form>
  );
};

export default Form2;
