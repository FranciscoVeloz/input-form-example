import { ChangeEvent, FormEvent, useState } from "react";
import { IUser } from "../interfaces/IUser";
import { UserSchema } from "../schemas/user.schema";

//Estado inicial del form
const initialState: IUser = {
  fullname: "",
  email: "",
  password: "",
};

const Form1 = () => {
  //Use state para el form
  const [user, setUser] = useState<IUser>(initialState);

  //Manejamos el cambio de estado
  const handleChange = (e: ChangeEvent<HTMLInputElement>) =>
    setUser({ ...user, [e.target.name]: e.target.value });

  //Al dar click al boton enviamos los datos
  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()

    const result = UserSchema.safeParse(user);
    if (!result.success) {
      result.error.errors.forEach((e) => alert(e.message));
      return;
    }

    setUser(initialState);
    alert("Usuario agregado con éxito");
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="grid gap-6 mb-6">
        <input
          type="text"
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          placeholder="Fullname"
          name="fullname"
          onChange={handleChange}
          value={user.fullname}
          required
        />

        <input
          type="text"
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          placeholder="Email"
          name="email"
          onChange={handleChange}
          value={user.email}
          required
        />

        <input
          type="password"
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          placeholder="Password"
          name="password"
          onChange={handleChange}
          value={user.password}
          required
        />

        <button
          type="submit"
          className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
        >
          Submit
        </button>
      </div>
    </form>
  );
};

export default Form1;
