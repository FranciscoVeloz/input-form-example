import { z } from "zod";

export const UserSchema = z.object({
  fullname: z.string(),
  email: z.string().email(),
  password: z.string().min(8),
});
